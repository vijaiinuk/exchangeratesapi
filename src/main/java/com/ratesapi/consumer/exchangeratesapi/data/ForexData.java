package com.ratesapi.consumer.exchangeratesapi.data;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString
@ApiModel(description = "Forex data")
public class ForexData {

    private String baseCcy;

    private List<CcyData> ccyData = new ArrayList<>();

}

