package com.ratesapi.consumer.exchangeratesapi.data;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString
public class CcyData {

    private String ccy;

    private List<SpotData> spotData = new ArrayList<>();

}
