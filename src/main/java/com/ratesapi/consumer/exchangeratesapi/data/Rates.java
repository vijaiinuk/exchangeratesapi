package com.ratesapi.consumer.exchangeratesapi.data;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({

})
public class Rates {

    @JsonIgnore
    private Map<String, BigDecimal> additionalProperties = new HashMap<>();

    @JsonAnyGetter
    public Map<String, BigDecimal> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, BigDecimal value) {
        this.additionalProperties.put(name, value);
    }

}
