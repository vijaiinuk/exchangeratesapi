package com.ratesapi.consumer.exchangeratesapi.service;

import com.ratesapi.consumer.exchangeratesapi.data.ApiResponse;
import org.springframework.cache.annotation.Cacheable;

public interface ExternalApiServiceI {
    @Cacheable(value = "apiResponses", key = "#url", unless = "#result == null ")
    ApiResponse fetchExchangeRate(String url);
}
