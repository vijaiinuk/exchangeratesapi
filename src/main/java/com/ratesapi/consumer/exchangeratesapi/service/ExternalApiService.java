package com.ratesapi.consumer.exchangeratesapi.service;

import com.ratesapi.consumer.exchangeratesapi.data.ApiResponse;
import com.ratesapi.consumer.exchangeratesapi.exception.InternalServerException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class ExternalApiService implements ExternalApiServiceI {

    private final RestTemplate restTemplate;

    @Autowired
    public ExternalApiService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    @Cacheable(value = "apiResponses", key = "#url", unless = "#result == null ")
    public ApiResponse fetchExchangeRate(String url) {

        ApiResponse apiResponse = null;
        log.info("Connecting the external url: {}", url);
        ResponseEntity<ApiResponse> response = restTemplate.exchange(url, HttpMethod.GET, getHttpEntity(), ApiResponse.class);

        if(response.getStatusCode() == HttpStatus.OK) {
            apiResponse = response.getBody();
        } else {
            log.error("External API Request failed with response: "+response);
            log.error("Response body: {}", response.getBody());
            throw new InternalServerException("External API Service unavailable or having issues with status: "+response.getStatusCode());
        }

        return apiResponse;
    }
    private HttpEntity<String> getHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", "REST Service");

        return new HttpEntity<>(headers);
    }
}
