package com.ratesapi.consumer.exchangeratesapi.service;

import com.ratesapi.consumer.exchangeratesapi.data.ApiResponse;
import com.ratesapi.consumer.exchangeratesapi.data.CcyData;
import com.ratesapi.consumer.exchangeratesapi.data.ForexData;
import com.ratesapi.consumer.exchangeratesapi.utils.AppUtils;
import com.ratesapi.consumer.exchangeratesapi.validator.InputValidatorI;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class ExchangeRateApiService implements ExchangeRateApiServiceI {


    private final InputValidatorI inputValidator;

    private final ExternalApiServiceI externalApiService;

    private final String externalApiUrl;

    private final AppUtils appUtils;



    @Autowired
    public ExchangeRateApiService(InputValidatorI inputValidator,
                                  ExternalApiServiceI externalApiService,
                                  @Value("${app.rates.api.url}") String externalApiUrl,
                                  AppUtils appUtils) {
        this.inputValidator = inputValidator;
        this.externalApiService = externalApiService;
        this.externalApiUrl = externalApiUrl;
        this.appUtils = appUtils;
    }
    @Override
    public ForexData exchangeRateLatest(String baseCcy, List<String> currencies) {
        return exchangeRate(true, null, baseCcy, currencies);
    }

    @Override
    public ForexData exchangeRate(boolean latest, String date, String baseCcy, List<String> currencies) {

        String formattedDate = "";
        List<String> pastSixDates = null;
        if(!latest)  {
            formattedDate = inputValidator.validateDate(date);
            pastSixDates = appUtils.getPastSixMonthsSameDate(formattedDate);
        }
        inputValidator.validateCcy(baseCcy);
        inputValidator.validateCurrencyList(currencies);


        List<String> apiUrls = new ArrayList<>();

        if(latest)  {
            apiUrls.add(appUtils.getFormattedUrl(externalApiUrl+"latest", baseCcy, currencies));
        }
        else  {
            pastSixDates.forEach(d -> {
                apiUrls.add(appUtils.getFormattedUrl(externalApiUrl+d, baseCcy, currencies));
            });
        }

        ForexData forexData = new ForexData();

        Map<String, CcyData> ccyDataMap = new ConcurrentHashMap<>();

        apiUrls.forEach(url -> {

            ApiResponse apiResponse = externalApiService.fetchExchangeRate(url);

            log.info("ApiResponse got: {}", apiResponse);

            forexData.setBaseCcy(baseCcy);
            if(apiResponse != null) {
                appUtils.getCcyData(apiResponse, ccyDataMap);
            } else {
                log.error("External API Request failed");
            }
        });
        forexData.getCcyData().addAll(ccyDataMap.values());

        return forexData;
    }
}
