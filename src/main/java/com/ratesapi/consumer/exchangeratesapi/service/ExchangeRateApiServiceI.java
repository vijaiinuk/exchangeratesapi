package com.ratesapi.consumer.exchangeratesapi.service;

import com.ratesapi.consumer.exchangeratesapi.data.ForexData;

import java.util.List;

public interface ExchangeRateApiServiceI {
    ForexData exchangeRateLatest(String baseCcy, List<String> currencies);

    ForexData exchangeRate(boolean latest, String date, String baseCcy, List<String> currencies);
}
