package com.ratesapi.consumer.exchangeratesapi.validator;

import java.util.Collection;

public interface InputValidatorI {
    String validateDate(String dateStr);

    void validateCcy(String ccy);

    void validateCurrencyList(Collection<String> currencies);
}
