package com.ratesapi.consumer.exchangeratesapi.validator;

import com.ratesapi.consumer.exchangeratesapi.exception.InvalidInputException;
import com.ratesapi.consumer.exchangeratesapi.utils.AppUtils;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.Currency;

import static org.apache.logging.log4j.util.Strings.isBlank;

@Component
public class InputValidator implements InputValidatorI {

    @Override
    public String validateDate(String dateStr)  {
        String formattedDate = null;
        boolean isValid = true;
        if(isBlank(dateStr))
            isValid = false;
        else {
            try  {
                LocalDate date = LocalDate.parse(dateStr, AppUtils.USER_FORMAT);
                formattedDate = date.format(AppUtils.HYPEN_FORMAT);
            } catch (DateTimeParseException e)  {
                isValid = false;
            }
        }


        if(!isValid)
            throw new InvalidInputException("Invalid Date String provided");

        return formattedDate;
    }

    @Override
    public void validateCcy(String ccy) {
        if(isBlank(ccy))  {
            throw new InvalidInputException("Empty Currency String provided");
        } else {
            try {
                Currency.getInstance(ccy);
            } catch (IllegalArgumentException e)  {
                throw new InvalidInputException("Invalid Currency String provided: "+ccy);
            }
        }
    }

    @Override
    public void validateCurrencyList(Collection<String> currencies)  {
        if(currencies != null && currencies.size() > 0) {
            try {
                currencies.forEach(this::validateCcy);
            } catch (InvalidInputException e) {
                throw new InvalidInputException("Invalid Currency list provided.  Please check");
            }
        }
    }
}
