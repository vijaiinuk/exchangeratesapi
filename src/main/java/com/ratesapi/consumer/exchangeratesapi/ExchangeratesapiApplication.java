package com.ratesapi.consumer.exchangeratesapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class ExchangeratesapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExchangeratesapiApplication.class, args);
	}

}
