package com.ratesapi.consumer.exchangeratesapi.error;

import com.ratesapi.consumer.exchangeratesapi.exception.InternalServerException;
import com.ratesapi.consumer.exchangeratesapi.exception.InvalidInputException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({InvalidInputException.class})
    public ResponseEntity<Object> handleBadRequest(final InvalidInputException exception, final WebRequest request)  {
        return handleExceptionInternal(exception, exception.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
    @ExceptionHandler({InternalServerException.class})
    public ResponseEntity<Object> handleAPIException(final InternalServerException exception, final WebRequest request)  {
        return handleExceptionInternal(exception, exception.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
