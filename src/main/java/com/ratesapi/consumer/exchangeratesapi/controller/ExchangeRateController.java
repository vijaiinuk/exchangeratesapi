package com.ratesapi.consumer.exchangeratesapi.controller;

import com.ratesapi.consumer.exchangeratesapi.data.ForexData;
import com.ratesapi.consumer.exchangeratesapi.service.ExchangeRateApiServiceI;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.ratesapi.consumer.exchangeratesapi.utils.APIConstants.URI_PATH_FETCH_HISTORICAL;
import static com.ratesapi.consumer.exchangeratesapi.utils.APIConstants.URI_PATH_FETCH_LATEST;

@Api(value = "Exchange Rates System")
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successfully retrieved list"),
        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
        @ApiResponse(code = 500, message = "The server encountered an error. Please try again or contact support")
})
@RestController
public class ExchangeRateController {

    private final ExchangeRateApiServiceI service;

    @Autowired
    public ExchangeRateController(ExchangeRateApiServiceI service) {
        this.service = service;
    }

    @ApiOperation(value = "Fetches the Latest Forex rate for the given currencies")
    @GetMapping(URI_PATH_FETCH_LATEST)
    public ResponseEntity<ForexData> exchangeRateForToday(@ApiParam(value = "Base Currency (Default Euro)")
                                                              @RequestParam(defaultValue = "EUR") String baseCcy,
                                                          @ApiParam(value = "List of currencies")
                                                          @RequestParam(required = false) List<String> currencies)  {
        return ResponseEntity.ok(service.exchangeRateLatest(baseCcy, currencies));
    }

    @ApiOperation(value = "Fetches the Historic (past 6 months - same input date) Forex rate for the given currencies")
    @GetMapping(URI_PATH_FETCH_HISTORICAL)
    public ResponseEntity<ForexData> exchangeRateForHistoricalDate(@ApiParam(value = "Spot Date (in YYYYMMDD format)", defaultValue = "20200423")
                                                                       @RequestParam(required = true) String date,
                                                                   @ApiParam(value = "Base Currency (Default Euro)")
                                                                   @RequestParam(defaultValue = "EUR") String baseCcy,
                                                                   @ApiParam(value = "List of currencies")
                                                                       @RequestParam(required = false) List<String> currencies)  {
        return ResponseEntity.ok(service.exchangeRate(false, date, baseCcy, currencies));
    }
}
