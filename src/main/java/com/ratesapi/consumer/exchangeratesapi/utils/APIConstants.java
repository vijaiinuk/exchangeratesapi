package com.ratesapi.consumer.exchangeratesapi.utils;

public class APIConstants {

    public static final String URI_PATH_FETCH_LATEST = "/exchangeRateForToday";
    public static final String URI_PATH_FETCH_HISTORICAL = "/exchangeRateForHistoricalDate";
}
