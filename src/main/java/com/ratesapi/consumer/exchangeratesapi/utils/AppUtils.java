package com.ratesapi.consumer.exchangeratesapi.utils;

import com.ratesapi.consumer.exchangeratesapi.data.ApiResponse;
import com.ratesapi.consumer.exchangeratesapi.data.CcyData;
import com.ratesapi.consumer.exchangeratesapi.data.SpotData;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class AppUtils {

    public static final DateTimeFormatter HYPEN_FORMAT = DateTimeFormatter.ISO_LOCAL_DATE;

    public static final DateTimeFormatter USER_FORMAT = DateTimeFormatter.BASIC_ISO_DATE;


    public List<String> getPastSixMonthsSameDate(String formattedDate) {


        LocalDate localDate = LocalDate.parse(formattedDate, HYPEN_FORMAT);

        return IntStream.range(0, 6)
                .mapToObj(i -> localDate.minusMonths(i).format(HYPEN_FORMAT)).collect(Collectors.toList());
    }


    public String getFormattedUrl(String inputString, String baseCcy, List<String> currencies) {

        StringBuilder sb = new StringBuilder(inputString);
        if(baseCcy != null || currencies.size() > 0)
            sb.append("?");

        if(baseCcy != null)
            sb.append("base=").append(baseCcy);
        if(currencies != null && currencies.size() > 0)  {
            if(baseCcy != null)
                sb.append("&");
            sb.append("symbols=").append(String.join(",", currencies));
        }
        return sb.toString();
    }

    public void getCcyData(ApiResponse apiResponse, Map<String, CcyData> ccyDataMap) {

        if(apiResponse != null)  {
            if(apiResponse.getRates() != null)  {
                String date = apiResponse.getDate();
                apiResponse.getRates().getAdditionalProperties().forEach((k,v) -> {
                    CcyData ccyData = ccyDataMap.getOrDefault(k, new CcyData());
                    ccyData.setCcy(k);
                    SpotData spotData = new SpotData();
                    spotData.setDate(date);
                    spotData.setSpotRate(v);

                    ccyData.getSpotData().add(spotData);
                    ccyDataMap.putIfAbsent(k, ccyData);

                });
            }
        }
    }

}
