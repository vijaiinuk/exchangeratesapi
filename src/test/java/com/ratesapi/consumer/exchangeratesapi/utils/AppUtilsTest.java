package com.ratesapi.consumer.exchangeratesapi.utils;

import com.ratesapi.consumer.exchangeratesapi.data.ApiResponse;
import com.ratesapi.consumer.exchangeratesapi.data.CcyData;
import com.ratesapi.consumer.exchangeratesapi.data.Rates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

class AppUtilsTest {

    private AppUtils appUtils;

    @BeforeEach
    void setUp() {
        appUtils = new AppUtils();
    }

    @Test
    public void pastDates()  {

        List<String> pastDates = appUtils.getPastSixMonthsSameDate("2020-04-20");

        assertEquals(6, pastDates.size());

        assertEquals(asList("2020-04-20", "2020-03-20", "2020-02-20", "2020-01-20", "2019-12-20", "2019-11-20"), pastDates);
    }

    @Test
    void getFormattedUrl() {

        assertEquals("http://someurl?base=EUR&symbols=USD,GBP",
                appUtils.getFormattedUrl("http://someurl", "EUR", asList("USD", "GBP")));

        assertEquals("http://someurl?base=EUR&symbols=GBP",
                appUtils.getFormattedUrl("http://someurl", "EUR", asList("GBP")));

        assertEquals("http://someurl?symbols=GBP",
                appUtils.getFormattedUrl("http://someurl", null, asList("GBP")));

        assertEquals("http://someurl?base=EUR",
                appUtils.getFormattedUrl("http://someurl", "EUR", Collections.emptyList()));
    }

    @Test
    void getCcyData() {

        ConcurrentHashMap<String, CcyData> ccyDataMap = new ConcurrentHashMap<>();
        appUtils.getCcyData(new ApiResponse(), ccyDataMap);
        assertEquals(0, ccyDataMap.size());
    }
    @Test
    void getCcyData_1() {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setBase("EUR");
        apiResponse.setDate("2020-04-23");
        Rates rates = new Rates();
        Map<String, BigDecimal> additionalProperties = new HashMap<>();
        additionalProperties.put("GBP", new BigDecimal("1.11"));
        additionalProperties.put("USD", new BigDecimal("1.5"));
        rates.setAdditionalProperties(additionalProperties);
        apiResponse.setRates(rates);
        ConcurrentHashMap<String, CcyData> ccyDataMap = new ConcurrentHashMap<>();
        appUtils.getCcyData(apiResponse, ccyDataMap);
        assertEquals(2, ccyDataMap.size());
        assertEquals("2020-04-23", ccyDataMap.get("GBP").getSpotData().get(0).getDate());
        assertEquals(new BigDecimal("1.11"), ccyDataMap.get("GBP").getSpotData().get(0).getSpotRate());
    }
}