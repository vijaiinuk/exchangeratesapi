package com.ratesapi.consumer.exchangeratesapi.service;

import com.ratesapi.consumer.exchangeratesapi.data.ApiResponse;
import com.ratesapi.consumer.exchangeratesapi.exception.InternalServerException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ExternalApiServiceI.class)
class ExternalApiServiceTest {

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private ExternalApiServiceI apiService;

    @Test
    void fetchExchangeRate() {
        ApiResponse apiResponse = new ApiResponse();

        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(ApiResponse.class)))
            .thenReturn(ResponseEntity.ok(apiResponse));

        assertEquals(apiResponse, apiService.fetchExchangeRate("anystring"));

    }
    @Test
    void fetchExchangeRate_externalapi_failure() {
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(ApiResponse.class)))
                .thenReturn(ResponseEntity.badRequest().build());

        assertThrows(InternalServerException.class, () -> apiService.fetchExchangeRate("anystring"));
    }

}