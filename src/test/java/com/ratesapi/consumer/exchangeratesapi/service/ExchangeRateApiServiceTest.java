package com.ratesapi.consumer.exchangeratesapi.service;

import com.ratesapi.consumer.exchangeratesapi.data.ApiResponse;
import com.ratesapi.consumer.exchangeratesapi.utils.AppUtils;
import com.ratesapi.consumer.exchangeratesapi.validator.InputValidatorI;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ExchangeRateApiService.class)
public class ExchangeRateApiServiceTest {

    @MockBean
    private InputValidatorI inputValidator;

    @MockBean
    private AppUtils appUtils;

    @MockBean
    private ExternalApiServiceI externalApiService;

    @Autowired
    private ExchangeRateApiServiceI apiService;

    @Test
    public void exchangeRate()  {
        when(externalApiService.fetchExchangeRate(anyString())).thenReturn(new ApiResponse());
        assertNotNull(apiService.exchangeRateLatest("EUR", asList("USD")));
    }
}
