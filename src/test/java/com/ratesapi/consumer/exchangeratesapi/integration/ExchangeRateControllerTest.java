package com.ratesapi.consumer.exchangeratesapi.integration;

import com.ratesapi.consumer.exchangeratesapi.controller.ExchangeRateController;
import com.ratesapi.consumer.exchangeratesapi.data.ApiResponse;
import com.ratesapi.consumer.exchangeratesapi.service.ExchangeRateApiServiceI;
import com.ratesapi.consumer.exchangeratesapi.service.ExternalApiServiceI;
import com.ratesapi.consumer.exchangeratesapi.utils.AppUtils;
import com.ratesapi.consumer.exchangeratesapi.validator.InputValidatorI;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import static com.ratesapi.consumer.exchangeratesapi.utils.APIConstants.URI_PATH_FETCH_HISTORICAL;
import static com.ratesapi.consumer.exchangeratesapi.utils.APIConstants.URI_PATH_FETCH_LATEST;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc(addFilters = false)
@WebMvcTest({ExchangeRateController.class, ExchangeRateApiServiceI.class, ExternalApiServiceI.class, InputValidatorI.class, AppUtils.class})
public class ExchangeRateControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RestTemplate restTemplate;

    @Test
    public void fetchCurrentDaysRate_validparams_returns_200() throws Exception {
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(ApiResponse.class)))
                .thenReturn(ResponseEntity.status(HttpStatus.OK).build());
        RequestBuilder request = MockMvcRequestBuilders.get(URI_PATH_FETCH_LATEST+"?baseCcy=EUR&currencies=USD");
        mockMvc.perform(request).andDo(print()).andExpect(status().isOk());

        request = MockMvcRequestBuilders.get(URI_PATH_FETCH_LATEST);
        mockMvc.perform(request).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void fetchCurrentDaysRate_servererror_returns_5xx() throws Exception {
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(ApiResponse.class)))
                .thenReturn(ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build());
        RequestBuilder request = MockMvcRequestBuilders.get(URI_PATH_FETCH_LATEST+"?baseCcy=EUR&currencies=USD");
        mockMvc.perform(request).andDo(print()).andExpect(status().isInternalServerError());
    }

    @Test
    public void fetchHistoricalDaysRate_noparam_or_badparam_returns_400() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get(URI_PATH_FETCH_HISTORICAL);
        mockMvc.perform(request).andDo(print()).andExpect(status().isBadRequest());

        request = MockMvcRequestBuilders.get(URI_PATH_FETCH_HISTORICAL+"?baseCcy=EUR");
        mockMvc.perform(request).andDo(print()).andExpect(status().isBadRequest());

        request = MockMvcRequestBuilders.get(URI_PATH_FETCH_HISTORICAL+"?currencies=USD");
        mockMvc.perform(request).andDo(print()).andExpect(status().isBadRequest());

    }

    @Test
    public void fetchHistoricalDaysRate_validparams_returns_200() throws Exception {
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(ApiResponse.class)))
                .thenReturn(ResponseEntity.status(HttpStatus.OK).build());
        RequestBuilder request = MockMvcRequestBuilders.get(URI_PATH_FETCH_HISTORICAL+"?date=20200423&baseCcy=EUR&currencies=USD");
        mockMvc.perform(request).andDo(print()).andExpect(status().isOk());

        request = MockMvcRequestBuilders.get(URI_PATH_FETCH_HISTORICAL+"?date=20200423");
        mockMvc.perform(request).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void fetchHistoricalDaysRate_servererror_returns_5xx() throws Exception {
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(ApiResponse.class)))
                .thenReturn(ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build());
        RequestBuilder request = MockMvcRequestBuilders.get(URI_PATH_FETCH_HISTORICAL+"?date=20200423&baseCcy=EUR&currencies=USD");
        mockMvc.perform(request).andDo(print()).andExpect(status().isInternalServerError());
    }


    @Test
    public void fetchHistoricalDaysRate_calls_externalapi_sixtimes() throws Exception {
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(ApiResponse.class)))
                .thenReturn(ResponseEntity.status(HttpStatus.OK).build());

        RequestBuilder request = MockMvcRequestBuilders.get(URI_PATH_FETCH_HISTORICAL+"?date=20200423&baseCcy=EUR&currencies=USD");
        mockMvc.perform(request).andDo(print()).andExpect(status().isOk());
        verify(restTemplate, times(6)).exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(ApiResponse.class));

        request = MockMvcRequestBuilders.get(URI_PATH_FETCH_HISTORICAL+"?date=20200423");
        mockMvc.perform(request).andDo(print()).andExpect(status().isOk());
    }

}
