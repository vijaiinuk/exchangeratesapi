package com.ratesapi.consumer.exchangeratesapi.validator;

import com.ratesapi.consumer.exchangeratesapi.exception.InvalidInputException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class InputValidatorTest {

    private InputValidatorI validator;

    @BeforeEach
    void setUp() {
        validator = new InputValidator();
    }

    @Test
    void validDate_invalidInputs_throws_exception() {
        assertThrows(InvalidInputException.class, () -> validator.validateDate(""));
        assertThrows(InvalidInputException.class, () -> validator.validateDate(null));
        assertThrows(InvalidInputException.class, () -> validator.validateDate("abcd"));
        assertThrows(InvalidInputException.class, () -> validator.validateDate("20202904"));
    }

    @Test
    void validDate_validInputs_doesnot_throw() {
        assertDoesNotThrow(() -> validator.validateDate("20200420"));
    }

    @Test
    void validCcy_invalidCurrency_throws_exception() {
        assertThrows(InvalidInputException.class, () -> validator.validateCcy(null));
        assertThrows(InvalidInputException.class, () -> validator.validateCcy(""));
        assertThrows(InvalidInputException.class, () -> validator.validateCcy(" "));

        assertThrows(InvalidInputException.class, () -> validator.validateCcy("abc"));
    }


    @Test
    void validCcy() {
        assertDoesNotThrow(() -> validator.validateCcy("USD"));
    }




    @Test
    void validateCurrencyList()  {
        assertDoesNotThrow(() -> validator.validateCurrencyList(null));
        assertDoesNotThrow(() -> validator.validateCurrencyList(Collections.emptyList()));

        assertThrows(InvalidInputException.class, () -> validator.validateCurrencyList(Collections.singletonList("TEST")));
        assertThrows(InvalidInputException.class, () -> validator.validateCurrencyList(asList("USD", "TEST")));
        assertThrows(InvalidInputException.class, () -> validator.validateCurrencyList(asList("TEST", "EUR")));

        assertDoesNotThrow(() -> validator.validateCurrencyList(Collections.singletonList("USD")));
    }
}