###### README

This is a Spring Boot REST API service for fetching the Forex data from the API - https://exchangeratesapi.io/

There are 2 endpoints 
1. To fetch the latest Forex Data
2. To fetch the historical/past 6 months' data.

**Input Parameters:**
1. Base Currency - on which to get the Fx data for.  This is defaulted to EURO
2. Currencies - list of currencies for which the Fx data is needed for.
    Base currency and Currencies code will be validated against the Currency ISO Code.  Full list at http://www.iso.org/iso/home/standards/currency_codes.htm
3. Date - in YYYYMMDD format.  Date for which FX data is requested for.  And the last 6 months data for the same day will be fetched.

**How To Build:**
1. Download the source code from git.
2. run the below command to compile/test and generate the jar file:
`mvn clean install`
3. start the application by running the below command:
`java -jar target/exchangeratesapi-0.0.1-SNAPSHOT.jar`
4. This should start the application: 
api-docs : http://localhost:8000/v2/api-docs
5. Swagger url:
http://localhost:8000/swagger-ui.html
6. use credentials admin/admin if prompted.


**Points to note:**

Caching the api results:

The external api requests are cached in-memory, hence the same data requested multiple times will be served from the cache (rather than fetching from the external api)

Sample curl commands:

`curl -u admin:admin -X GET "http://localhost:8000/exchangeRateForHistoricalDate?baseCcy=EUR&date=20200423" -H "accept: */*"`

`curl -u admin:admin -X GET "http://localhost:8000/exchangeRateForToday?baseCcy=EUR&currencies=USD&currencies=GBP" -H "accept: */*"`

